package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.service.IUserService
import fr.eplicgames.android.learnrun.service.call
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment(abstractActivity: AbstractActivity) :
    FragmentActivityAccess(abstractActivity) {

    override val layoutResource = R.layout.fragment_login

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        connectionButton.setPushAndOnClick {
            service<IUserService>()
                .login(
                    emailFieldView.text, passwordFieldView.text, true
                ).call {
                    onSuccess = { mainActivity.onLoginSuccess() }
                    onUserError = { toast(getString(R.string.wrong_password)) }
                    onServerError = { toast(getString(R.string.server_error)) }
                    onFailure = { toast(getString(R.string.connection_failure)) }
                }
        }
        emailFieldView.onEditDone = { passwordFieldView.requestEditMode() }
        passwordFieldView.onEditDone = { hideKeyBoard() }


        forgotPasswordTextView.setPushAndOnClick {
            mainActivity.pushFragment<ForgotPasswordFragment>()
        }
        subscribeTextView.setPushAndOnClick {
            mainActivity.pushFragment<ChooseCreateAccountFragment>()
        }
    }
}
