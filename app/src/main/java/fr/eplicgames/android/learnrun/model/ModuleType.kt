package fr.eplicgames.android.learnrun.model

import android.graphics.Color

enum class ModuleType(
    val color: Int,
    val displayName: String
) {
    GENERAL(Color.rgb(41, 47, 51), "Général"),

    MATHS(Color.rgb(81, 196, 127), "Mathématiques"),

    FRANCAIS(Color.rgb(28, 124, 172), "Français"),

    PHYSIQUE(Color.rgb(211, 127, 80), "Physique"),

    CHIMIE(Color.rgb(219, 152, 61), "Chimie"),

    ANGLAIS(Color.rgb(205, 68, 75), "Anglais"),

    HISTOIRE(Color.rgb(206, 128, 178), "Histoire"),

    GEOGRAPHIE(Color.rgb(165, 97, 158), "Géographie")
}