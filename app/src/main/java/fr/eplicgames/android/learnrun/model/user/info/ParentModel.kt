package fr.eplicgames.android.learnrun.model.user.info

import fr.eplicgames.android.learnrun.model.UserType

data class ParentModel(
    override val pseudo: String,
    override val firstname: String,
    override val lastname: String,
    override val email: String
) : IUserModel {
    override fun getUserType(): UserType = UserType.PARENT
}