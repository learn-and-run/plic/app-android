package fr.eplicgames.android.learnrun.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.model.holder.RelationDto
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.row_home_parent.view.*

class HomeParentRecyclerAdapter(
    val context: Context,
    val data: MutableList<RelationDto>,
    val onItemClick: (position: Int, relation: RelationDto, view: View) -> Unit) :
    RecyclerView.Adapter<HomeParentRecyclerAdapter.ViewHolder>() {
    // the new RecyclerAdapter enforces the use of
    // the ViewHolder class performance pattern
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val firstNameText: TextView = itemView.textHomeParentRowFirstname
    }
    override fun getItemCount(): Int {
        return data.size
    }
    // called when a new viewholder is required to display a row
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : ViewHolder {
        // create the row from a layout inflater
        val rowView = LayoutInflater
            .from(context)
            .inflate(R.layout.row_home_parent, parent, false)
        rowView.setPushAndOnClick {
            val position = it.tag as Int
            onItemClick(position, data[position], it)
        }
        // create a ViewHolder using this rowview
        val viewHolder =
            ViewHolder(
                rowView
            )
        // return this ViewHolder. The system will make sure view holders
        // are used and recycled
        return viewHolder
    }
    // called when a row is about to be displayed
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // retrieve the item at the specified position
        val currentItem = data[position]
        // put the data
        holder.firstNameText.text = currentItem.userInfoDto.firstname

        holder.itemView.tag = position
    }
}