package fr.eplicgames.android.learnrun.model

import fr.eplicgames.android.learnrun.model.user.info.ParentModel
import fr.eplicgames.android.learnrun.model.user.info.ProfessorModel
import fr.eplicgames.android.learnrun.model.user.info.StudentModel

enum class UserType(val modelType: Class<*>) {
    STUDENT (StudentModel::class.java),
    PARENT(ParentModel::class.java),
    PROFESSOR(ProfessorModel::class.java)
}