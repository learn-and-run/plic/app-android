package fr.eplicgames.android.learnrun.service

import fr.eplicgames.android.learnrun.model.ModuleType
import fr.eplicgames.android.learnrun.model.holder.ScoreByModuleAndPseudo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IScoreService {

    @GET("score/module")
    fun getCircuitsScores(
        @Query("pseudo") pseudo: String,
        @Query("moduleType") moduleType: ModuleType
    ): Call<ScoreByModuleAndPseudo>

}