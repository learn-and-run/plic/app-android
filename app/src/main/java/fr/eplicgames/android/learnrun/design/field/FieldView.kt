package fr.eplicgames.android.learnrun.design.field

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import androidx.constraintlayout.widget.ConstraintLayout
import fr.eplicgames.android.learnrun.R
import kotlinx.android.synthetic.main.field_view.view.*


class FieldView
    @JvmOverloads
    constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : ConstraintLayout(context, attrs, defStyleAttr) {


    var onEditEnter: () -> Unit = { }
    var onEditDone: () -> Boolean = { false }
    var onTextChanged: (text: String) -> Unit = {}

    init {
        inflate(context, R.layout.field_view, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.FieldView)
        fieldEditText.hint = attributes.getString(R.styleable.FieldView_hint)
        fieldEditText.inputType = attributes.getInt(
            R.styleable.FieldView_android_inputType,
            InputType.TYPE_CLASS_TEXT)
        attributes.recycle()

        fieldEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                onTextChanged(text)
            }
        })

        fieldEditText.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH, EditorInfo.IME_ACTION_DONE -> fieldEditText.clearFocus()
            }
            false
        }
        fieldEditText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus)
                onEditEnter()
            else
                onEditDone()
        }
    }

    fun requestEditMode() = fieldEditText.requestFocus()

    var text : String
        get() = fieldEditText?.text?.toString() ?: ""
        set(value) { fieldEditText.text = SpannableStringBuilder(value) }

}