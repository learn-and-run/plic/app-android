package fr.eplicgames.android.learnrun.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import fr.eplicgames.android.learnrun.R


class SpinnerArrayAdapter<T>(
    val context: Context,
    var baseView: TextView,
    val items: List<T>,
    val getDisplayName: T.() -> String,
    val getBackColor: (T.() -> Int)? = null,
    val onCurrentChange: ((T) -> Unit)? = null
) : BaseAdapter(), SpinnerAdapter {

    override fun getItem(position: Int) = items[position]
    override fun getItemId(position: Int) = position.toLong()
    override fun getCount() = items.size

    var currentPosition = 0

    fun getCurrentItem() = items[currentPosition]

    var itemViews: List<View> = items.mapIndexed { i, it ->
        View.inflate(context, R.layout.spinner_classe_item, null).apply {
            getBackColor?.let {color ->
                when(i) {
                    0 -> background = resources.getDrawable(R.drawable.round_up, context?.theme)
                    items.size - 1 -> background = resources.getDrawable(R.drawable.round_down, context?.theme)
                    else -> this.background = resources.getDrawable(R.drawable.rect, context?.theme)
                }
                this.background.setTint(it.color())
            }
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        currentPosition = position
        baseView.text = items[position].getDisplayName()
        onCurrentChange?.invoke(getCurrentItem())
        return baseView
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val textView = (itemViews[position] as TextView)
        if (getBackColor != null)
            textView.setTextColor(ContextCompat.getColor(context, R.color.white))
        textView.text = items[position].getDisplayName()
        return textView
    }
}