package fr.eplicgames.android.learnrun.model.holder

import fr.eplicgames.android.learnrun.model.ModuleType

data class CircuitScore(
    val circuitName: String,
    val moduleType: ModuleType,
    val score: Int,
    val spendTime: Int
)