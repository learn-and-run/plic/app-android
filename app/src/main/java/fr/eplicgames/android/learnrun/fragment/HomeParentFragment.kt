package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.adapter.HomeParentRecyclerAdapter
import fr.eplicgames.android.learnrun.decoration.SimpleDividerItemDecoration
import fr.eplicgames.android.learnrun.model.UserType
import fr.eplicgames.android.learnrun.service.IUserService
import fr.eplicgames.android.learnrun.service.call
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_home_parent.*

/**
 * A simple [Fragment] subclass.
 */
class HomeParentFragment(abstractActivity: AbstractActivity)
    : FragmentActivityAccess(abstractActivity) {

    override val layoutResource = R.layout.fragment_home_parent

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        home_parent_list.addItemDecoration(
            SimpleDividerItemDecoration(
                ContextCompat.getColor(context!!, R.color.colorPrimaryDark),
                2
            )
        )
        home_parent_list.setHasFixedSize(true)
        home_parent_list.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )

        service<IUserService>().getAllRelations(UserType.STUDENT).call {
            onSuccess = {
                home_parent_list.adapter = HomeParentRecyclerAdapter(
                    mainActivity,
                    it.body()?.toMutableList() ?: mutableListOf()
                ) { position, relation, view ->
                    toast(relation.userInfoDto.pseudo)
                    mainActivity.pushFragment(
                        fragment = StatsStudentFragment(mainActivity, relation.userInfoDto.pseudo),
                        addToBackStack = true
                    )
                }
            }
            onAnyErrorNoArg = {
                toast("Error while getting your relations")
                text_empty_list_HomeStudent.visibility = View.VISIBLE
            }
        }
        button_settings_home_student.setPushAndOnClick {
            mainActivity.pushFragment<ParameterFragment>()
        }



    }

}