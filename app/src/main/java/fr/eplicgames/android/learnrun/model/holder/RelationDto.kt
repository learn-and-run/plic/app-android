package fr.eplicgames.android.learnrun.model.holder

import fr.eplicgames.android.learnrun.model.UserType

data class RelationDto(
    val id: Long,
    val userInfoDto: UserInfoDto,
    val userType: UserType
)