package fr.eplicgames.android.learnrun.model.user.creation

import android.util.Patterns


interface IUserCreationModel {
    val pseudo: String
    val firstname: String
    val lastname: String
    val password: String
    val email: String

    fun isValid() : Boolean =
        pseudo.length    in 1..32 && pseudo.matches("^[^@]+$".toRegex()) &&
        firstname.length in 1..32 && firstname.matches("^[^@]+$".toRegex()) &&
        lastname.length  in 1..32 && lastname.matches("^[^@]+$".toRegex()) &&
        password.length  in 5..72 &&
                password.matches("^(?=.*\\d)(?=(.*\\W))(?=.*[a-zA-Z])(?!.*\\s).+\$".toRegex()) &&
        email.length  in 3..320 && email.matches(Patterns.EMAIL_ADDRESS.toRegex())
}