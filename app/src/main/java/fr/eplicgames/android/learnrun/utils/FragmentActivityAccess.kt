package fr.eplicgames.android.learnrun.utils

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import fr.eplicgames.android.learnrun.design.alert.AskAlertView
import fr.eplicgames.android.learnrun.design.alert.InfoAlertView
import fr.eplicgames.android.learnrun.model.user.info.IUserModel
import retrofit2.Retrofit

abstract class FragmentActivityAccess(protected val mainActivity: AbstractActivity) : Fragment() {

    abstract val layoutResource: Int
    val user: IUserModel
        get() = mainActivity.user
    val retrofit: Retrofit
        get() = mainActivity.retrofit

    fun toast(msg: String) = Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    fun toastLong(msg: String) = Toast.makeText(this.context, msg, Toast.LENGTH_LONG).show()

    inline fun <reified T> service(): T = retrofit.create(T::class.java)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        // Inflate the layout for this fragment
        return inflater.inflate(layoutResource, container, false)
    }

    fun hideKeyBoard() = mainActivity.hideKeyBoard()

    fun alertInfoDialog(message: String, textOk: String? = null): InfoAlertView {
        val alertView = InfoAlertView(context!!, null, 0)
        alertView.text = message
        if (textOk != null)
            alertView.textOk = textOk
        val alert = AlertDialog.Builder(mainActivity)
            .setView(alertView)
            .create()
        alertView.alertDialog = alert
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert.show()
        return alertView
    }

    fun alertAskDialog(message: String, textYes: String? = null, textNo: String?): AskAlertView {
        val alertView = AskAlertView(context!!, null, 0)
        alertView.text = message
        if (textYes != null)
            alertView.textYes = textYes
        if (textNo != null)
            alertView.textNo = textNo
        val alert = AlertDialog.Builder(mainActivity)
            .setView(alertView)
            .create()
        alertView.alertDialog = alert
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert.show()
        return alertView
    }

    fun Int.dp(): Int {
        val scale = context!!.resources.displayMetrics.density
        val pixels = this * scale + 0.5f
        return pixels.toInt()
    }

}