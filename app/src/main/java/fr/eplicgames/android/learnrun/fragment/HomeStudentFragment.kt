package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.adapter.SpinnerArrayAdapter
import fr.eplicgames.android.learnrun.adapter.HomeRecyclerAdapter
import fr.eplicgames.android.learnrun.decoration.SimpleDividerItemDecoration
import fr.eplicgames.android.learnrun.design.spinner.CustomSpinner
import fr.eplicgames.android.learnrun.model.ModuleType
import fr.eplicgames.android.learnrun.model.holder.CircuitScore
import fr.eplicgames.android.learnrun.service.IScoreService
import fr.eplicgames.android.learnrun.service.IUserService
import fr.eplicgames.android.learnrun.service.call
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_home_student.*

/**
 * A simple [Fragment] subclass.
 */
class HomeStudentFragment(abstractActivity: AbstractActivity) :
    FragmentActivityAccess(abstractActivity) {

    override val layoutResource = R.layout.fragment_home_student

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        class_spinnerHomeStudent.setPopupBackgroundDrawable(
            resources.getDrawable(R.drawable.spinner_back_activate, context?.theme)
        )
        class_spinnerHomeStudent.dropDownVerticalOffset = 10.dp()

        val adapter = SpinnerArrayAdapter(
            context!!,
            spinnerTextViewHomeStudent,
            ModuleType.values().toList(),
            ModuleType::displayName,
            ModuleType::color
        ) { moduleType ->
            service<IScoreService>().getCircuitsScores(
                "Bigfoot"/*TODO replace with user.pseudo*/,
                moduleType
            ).call {
                onSuccess = {
                    listHomeStudent.adapter =
                        HomeRecyclerAdapter(
                            mainActivity,
                            it.body()?.scores?.toMutableList() ?: mutableListOf()
                        ) { row: Int, circuitScore: CircuitScore, _: View ->
                            toast("row = $row\ncircuitScore = $circuitScore")
                        }
                }
            }
        }
        class_spinnerHomeStudent.adapter = adapter


        class_spinnerHomeStudent.setSpinnerEventsListener(object :
            CustomSpinner.OnSpinnerEventsListener {
            override fun onSpinnerOpened(spinner: AppCompatSpinner?) {
                spinnerActivate()
            }

            override fun onSpinnerClosed(spinner: AppCompatSpinner?) {
                spinnerDeactivate()
            }
        })

        listHomeStudent.addItemDecoration(
            SimpleDividerItemDecoration(
                ContextCompat.getColor(context!!, R.color.colorPrimaryDark),
                2
            )
        )

        listHomeStudent.setHasFixedSize(true)
        listHomeStudent.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )

        button_settings_home_student.setPushAndOnClick {
            service<IUserService>().logout().call {
                onSuccess = {
                    mainActivity.resetApp()
                    toast(getString(R.string.successfully_disconnected))
                }
                onFailure = { toast(getString(R.string.cant_contact_server)) }
                onAnyErrorNoArg = {
                    mainActivity.resetApp()
                }
            }
        }

        button_parent_home_student.setPushAndOnClick {
            //TODO
        }

        button_friends_home_student.setPushAndOnClick {
            //TODO
        }
    }

    fun spinnerDeactivate() {
        class_spinnerHomeStudent.background =
            resources.getDrawable(R.drawable.spinner_back_deactivate, context?.theme)
        //spinnerTextViewHomeStudent.visibility = View.VISIBLE
        class_spinnerHomeStudent.background.setTint(ModuleType.values()[class_spinnerHomeStudent.selectedItemPosition].color)
        chevronHomeStudent.setImageDrawable(resources.getDrawable(R.drawable.chevron_bottom, context?.theme))
    }

    fun spinnerActivate() {
        //class_spinnerHomeStudent.background =
        //    resources.getDrawable(R.drawable.spinner_back_activate, context?.theme)
        //spinnerTextViewHomeStudent.visibility = View.INVISIBLE
        chevronHomeStudent.setImageDrawable(resources.getDrawable(R.drawable.chevron_top, context?.theme))
    }
}
