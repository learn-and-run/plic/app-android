package fr.eplicgames.android.learnrun.model

enum class LevelType(
    val displayName: String
) {
    SIXIEME("Sixième"),
    CINQUIEME("Cinquième"),
    QUATRIEME("Quatrième"),
    TROISIEME("Toisième")
}
