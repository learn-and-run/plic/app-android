package fr.eplicgames.android.learnrun.model.holder

import fr.eplicgames.android.learnrun.model.LevelType

data class UserInfoDto(
    val pseudo: String,
    val firstname: String,
    val lastname: String,
    val email: String,
    val character: CharacterDto,
    val levelType: LevelType
)