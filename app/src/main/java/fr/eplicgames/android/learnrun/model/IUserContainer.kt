package fr.eplicgames.android.learnrun.model

import fr.eplicgames.android.learnrun.model.user.info.IUserModel

interface IUserContainer {
    var user: IUserModel
}