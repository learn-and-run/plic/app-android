package fr.eplicgames.android.learnrun.design.button

import android.content.Context
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import fr.eplicgames.android.learnrun.R
import kotlinx.android.synthetic.main.button_red.view.*

class ButtonRed
@JvmOverloads
constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.button_red, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.ButtonRed)
        buttonTextView.text = attributes.getString(R.styleable.ButtonRed_android_text)
        attributes.recycle()
    }

    var text : String
        get() = buttonTextView.text.toString()
        set(value) { buttonTextView.text = SpannableStringBuilder(value) }

    fun grayed(isGrayed: Boolean) {
        background = if (isGrayed) {
            resources.getDrawable(R.drawable.button_grayed, context.theme)
        } else {
            resources.getDrawable(R.drawable.button_red, context.theme)
        }
    }

}