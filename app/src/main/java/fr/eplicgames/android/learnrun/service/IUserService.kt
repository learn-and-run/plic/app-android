package fr.eplicgames.android.learnrun.service

import com.google.gson.JsonObject
import fr.eplicgames.android.learnrun.model.UserType
import fr.eplicgames.android.learnrun.model.holder.RelationDto
import fr.eplicgames.android.learnrun.model.user.creation.ParentCreationModel
import fr.eplicgames.android.learnrun.model.user.creation.ProfessorCreationModel
import fr.eplicgames.android.learnrun.model.user.creation.StudentCreationModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface IUserService {

    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("username")
        username: String,
        @Field("password")
        password: String,
        @Field("remember-me")
        rememberMe: Boolean
    ): Call<ResponseBody>

    @DELETE("logout")
    fun logout(): Call<ResponseBody>

    @GET("user/info")
    fun getUser(): Call<JsonObject>

    @POST("user/create_student")
    fun createStudent(@Body studentCreationModel: StudentCreationModel): Call<ResponseBody>

    @POST("user/create_parent")
    fun createParent(@Body parentCreationModel: ParentCreationModel): Call<ResponseBody>

    @POST("user/create_professor")
    fun createProfessor(@Body professorCreationModel: ProfessorCreationModel): Call<ResponseBody>

    @PATCH("user/update/pseudo")
    fun updatePseudo(@Body pseudoMap: Map<String, String>): Call<ResponseBody>

    @PATCH("user/update/email")
    fun updateEmail(@Body emailMap: Map<String, String>): Call<ResponseBody>

    @PATCH("user/update/password")
    fun updatePassword(@Body passwordMap: Map<String, String>): Call<ResponseBody>

    @POST("user/reset")
    fun askResetPassword(@Body emailMap: Map<String, String>): Call<ResponseBody>

    @DELETE("user/delete")
    fun deleteUser(): Call<ResponseBody>

    @GET("user/relation/all")
    fun getAllRelations(@Query("userType") userType: UserType): Call<List<RelationDto>>

}