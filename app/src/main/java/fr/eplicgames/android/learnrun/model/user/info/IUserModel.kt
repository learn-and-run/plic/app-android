package fr.eplicgames.android.learnrun.model.user.info

import fr.eplicgames.android.learnrun.model.UserType

interface IUserModel {
    val pseudo: String
    val firstname: String
    val lastname: String
    val email: String

    fun getUserType() : UserType
}