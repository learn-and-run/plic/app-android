package fr.eplicgames.android.learnrun.design.field

import android.content.Context
import android.text.InputType
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import com.thekhaeng.pushdownanim.PushDownAnim
import fr.eplicgames.android.learnrun.R
import kotlinx.android.synthetic.main.password_field_view.view.*

class PasswordFieldView
    @JvmOverloads
    constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var passwordHidden = true
    private var defaultInputType = 0

    var onEditEnter: () -> Unit = { }
    var onEditDone: () -> Boolean = { false }

    init {
        inflate(context, R.layout.password_field_view, this)

        if (!isInEditMode) {
            val attributes = context.obtainStyledAttributes(attrs, R.styleable.PasswordFieldView)
            passwordFieldEditText.hint = attributes.getString(R.styleable.PasswordFieldView_hint)
            defaultInputType = attributes.getInt(
                R.styleable.PasswordFieldView_android_inputType,
                InputType.TYPE_CLASS_TEXT
            )
            passwordFieldEditText.inputType = defaultInputType

            passwordFieldEditText.typeface = ResourcesCompat.getFont(context, R.font.chilanka_regular)

            PushDownAnim.setPushDownAnimTo(eyeImage).setOnClickListener {
                passwordHidden = !passwordHidden
                if (passwordHidden) {
                    eyeImage.setImageResource(R.drawable.eyelash)
                    passwordFieldEditText.inputType = defaultInputType
                } else {
                    eyeImage.setImageResource(R.drawable.eye)
                    passwordFieldEditText.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
                }
                passwordFieldEditText.typeface = ResourcesCompat.getFont(context, R.font.chilanka_regular)
            }
            attributes.recycle()

            passwordFieldEditText.setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_SEARCH, EditorInfo.IME_ACTION_DONE -> onEditDone()
                    else -> false
                }
            }
            passwordFieldEditText.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus)
                    onEditEnter()
            }
        }
    }

    fun requestEditMode() = passwordFieldEditText.requestFocus()

    var text : String
        get() = passwordFieldEditText?.text?.toString() ?: ""
        set(value) { passwordFieldEditText.text = SpannableStringBuilder(value) }


}