package fr.eplicgames.android.learnrun.model.user.creation

data class ProfessorCreationModel (
    override val pseudo: String,
    override val firstname: String,
    override val lastname: String,
    override val password: String,
    override val email: String
) : IUserCreationModel