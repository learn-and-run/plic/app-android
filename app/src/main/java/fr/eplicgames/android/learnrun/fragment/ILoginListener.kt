package fr.eplicgames.android.learnrun.fragment

interface ILoginListener {
    fun onLoginSuccess()
}