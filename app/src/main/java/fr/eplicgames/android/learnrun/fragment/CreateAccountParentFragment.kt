package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_create_account_parent.*


/**
 * A simple [Fragment] subclass.
 */
class CreateAccountParentFragment(abstractActivity: AbstractActivity) :
    FragmentActivityAccess(abstractActivity) {

    override val layoutResource = R.layout.fragment_create_account_parent

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        cancel_create_account_button.setPushAndOnClick {
            mainActivity.backActivity()
        }

    }


}
