package fr.eplicgames.android.learnrun.service

import okhttp3.ResponseBody
import retrofit2.Call

fun IUserService.askResetPassword(email: String): Call<ResponseBody> =
    this.askResetPassword(mapOf("email" to email))

fun IUserService.updatePseudo(pseudo: String): Call<ResponseBody> =
    this.updatePseudo(mapOf("pseudo" to pseudo))

fun IUserService.updateEmail(email: String): Call<ResponseBody> =
    this.updateEmail(mapOf("email" to email))

fun IUserService.updatePassword(password: String): Call<ResponseBody> =
    this.updatePassword(mapOf("password" to password))