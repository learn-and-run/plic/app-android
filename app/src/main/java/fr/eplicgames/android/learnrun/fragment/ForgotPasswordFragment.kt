package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import android.view.View
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.service.IUserService
import fr.eplicgames.android.learnrun.service.askResetPassword
import fr.eplicgames.android.learnrun.service.call
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_forgot_passsword.*

class ForgotPasswordFragment(mainActivity: AbstractActivity) :
    FragmentActivityAccess(mainActivity) {

    override val layoutResource = R.layout.fragment_forgot_passsword

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        forgotFieldView.onEditDone = { hideKeyBoard() }
        sendForgotButton.setPushAndOnClick {
            service<IUserService>().askResetPassword(forgotFieldView.text).call {
                onSuccess = {
                    alertInfoDialog(
                        getString(R.string.forgot_confirmation)
                                + "\n${forgotFieldView.text}"
                    )
                    mainActivity.backActivity()
                }
                onServerError = { toast(getString(R.string.server_error)) }
                onUserError = { toast(getString(R.string.enter_valid_email)) }
                onFailure = { toast(getString(R.string.connection_failure)) }
            }
        }
        cancelForgotButton.setPushAndOnClick { mainActivity.backActivity() }

    }

}
