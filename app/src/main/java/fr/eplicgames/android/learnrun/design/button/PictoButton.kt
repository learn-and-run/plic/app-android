package fr.eplicgames.android.learnrun.design.button

import android.content.Context
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import fr.eplicgames.android.learnrun.R
import kotlinx.android.synthetic.main.picto_button.view.*

class PictoButton
@JvmOverloads
constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.picto_button, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.PictoButton)
        buttonTextView.text = attributes.getString(R.styleable.PictoButton_android_text)
        imagePictoButton.setImageDrawable(attributes.getDrawable(R.styleable.PictoButton_srcCompat))

        attributes.recycle()
    }

    var text: String
        get() = buttonTextView.text.toString()
        set(value) {
            buttonTextView.text = SpannableStringBuilder(value)
        }

}