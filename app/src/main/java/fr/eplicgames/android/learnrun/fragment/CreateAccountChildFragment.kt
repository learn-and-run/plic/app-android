package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import androidx.appcompat.widget.AppCompatSpinner
import androidx.fragment.app.Fragment
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.adapter.SpinnerArrayAdapter
import fr.eplicgames.android.learnrun.design.spinner.CustomSpinner
import fr.eplicgames.android.learnrun.model.LevelType
import fr.eplicgames.android.learnrun.model.user.creation.StudentCreationModel
import fr.eplicgames.android.learnrun.service.IUserService
import fr.eplicgames.android.learnrun.service.call
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_create_account_child.*


/**
 * A simple [Fragment] subclass.
 */
class CreateAccountChildFragment(abstractActivity: AbstractActivity) :
    FragmentActivityAccess(abstractActivity) {

    override val layoutResource = R.layout.fragment_create_account_child

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        class_spinner.setPopupBackgroundDrawable(
            resources.getDrawable(R.drawable.spinner_back_activate, context?.theme)
        )
        class_spinner.dropDownHorizontalOffset = 160.dp()
        //class_spinner.dropDownVerticalOffset = (-47).dp()

        val adapter = SpinnerArrayAdapter(
            context!!,
            spinnerTextView,
            LevelType.values().toList(),
            LevelType::displayName
        )
        class_spinner.adapter = adapter

        class_spinner.setSpinnerEventsListener(object : CustomSpinner.OnSpinnerEventsListener {
            override fun onSpinnerOpened(spinner: AppCompatSpinner?) {
                spinnerActivate()
            }

            override fun onSpinnerClosed(spinner: AppCompatSpinner?) {
                spinnerDeactivate()
            }
        })


        cancel_create_account_button.setPushAndOnClick {
            mainActivity.backActivity()
        }

        create_account_button.setPushAndOnClick {
            val ask = alertAskDialog(
                "Es-tu sur de vouloir créer ce compte?",
                "Créer", "Annuler"
            )
            ask.onYes = {
                val user = StudentCreationModel(
                    pseudo_creation_field_view.text,
                    firstname_creation_field_view.text,
                    lastname_creation_field_view.text,
                    password_creation_field_view.text,
                    email_creation_field_view.text,
                    LevelType.valueOf(spinnerTextView.text.toString())
                )
                if (user.isValid()) {
                    service<IUserService>().createStudent(user).call {
                        onSuccess = {
                            val dialog =
                                alertInfoDialog("Un email de validation vous à été envoyé!\nConfirmez votre email")
                            dialog.onOk = {
                                mainActivity.resetApp()
                            }
                        }
                        onUserError =
                            { alertInfoDialog("Les informations entrées sont incorrecte\nOu l'email est déjà utilisé pour un autre compte") }
                        onServerError = { toast("Oops, une erreure est survenue!") }
                        onFailure = { toast("Impossible de contactez le serveur") }
                        onAnyError = { r, t -> Log.w("Error", errorAsCompactString(r, t)) }
                    }
                } else {
                    toast("Vous devez remplir tous les champs correctement")
                }
            }
        }

    }

    fun spinnerDeactivate() {
        class_spinner.background =
            resources.getDrawable(R.drawable.spinner_back_deactivate, context?.theme)
        spinnerTextView.visibility = VISIBLE
        chevron.setImageDrawable(resources.getDrawable(R.drawable.chevron_right, context?.theme))
    }

    fun spinnerActivate() {
        class_spinner.background =
            resources.getDrawable(R.drawable.spinner_back_activate, context?.theme)
        spinnerTextView.visibility = INVISIBLE
        chevron.setImageDrawable(resources.getDrawable(R.drawable.chevron_left, context?.theme))
    }


}
