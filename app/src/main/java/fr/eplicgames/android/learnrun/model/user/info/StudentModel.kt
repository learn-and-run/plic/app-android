package fr.eplicgames.android.learnrun.model.user.info

import fr.eplicgames.android.learnrun.model.LevelType
import fr.eplicgames.android.learnrun.model.UserType

data class StudentModel(
    override val pseudo: String,
    override val firstname: String,
    override val lastname: String,
    override val email: String,
    val levelType: LevelType
) : IUserModel {
    override fun getUserType(): UserType = UserType.STUDENT
}