package fr.eplicgames.android.learnrun.service

class ExceptionDuringSuccess(
    msg: String = "An exception occurred while the success callback was called"
) : Exception(msg)