package fr.eplicgames.android.learnrun.model.user.creation

data class ParentCreationModel (
    override val pseudo: String,
    override val firstname: String,
    override val lastname: String,
    override val password: String,
    override val email: String
) : IUserCreationModel