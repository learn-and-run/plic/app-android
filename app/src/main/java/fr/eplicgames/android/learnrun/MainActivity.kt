package fr.eplicgames.android.learnrun

import fr.eplicgames.android.learnrun.fragment.HomeParentFragment
import fr.eplicgames.android.learnrun.fragment.HomeStudentFragment
import fr.eplicgames.android.learnrun.fragment.LoginFragment
import fr.eplicgames.android.learnrun.model.UserType
import fr.eplicgames.android.learnrun.model.user.info.IUserModel
import fr.eplicgames.android.learnrun.service.ExceptionDuringSuccess
import fr.eplicgames.android.learnrun.service.IUserService
import fr.eplicgames.android.learnrun.service.call
import fr.eplicgames.android.learnrun.utils.AbstractActivity


class MainActivity : AbstractActivity() {

    override val apiUrl = "https://back-end-learn-run.herokuapp.com/api/"
    override val layoutResource = R.layout.activity_main

    override fun onInit() {
        checkConnection()
    }

    private fun checkConnection() {
        service<IUserService>().getUser().call {
            onSuccess = {
                val jsonResponse = it.body() ?: throw ExceptionDuringSuccess("User is null")
                val userType = UserType.valueOf(jsonResponse.get("userType").asString)
                user = gson.fromJson(jsonResponse.get("user"), userType.modelType) as IUserModel
                when(userType) {
                    UserType.STUDENT -> pushFragment<HomeStudentFragment>(false)
                    UserType.PARENT -> pushFragment<HomeParentFragment>(false)
                    UserType.PROFESSOR -> toast("Professors are not implemented yet")//TODO
                }
            }
            onAnyErrorNoArg = { pushFragment<LoginFragment>(false) }
        }
    }

    override fun onLoginSuccess() {
        checkConnection()
    }

}


