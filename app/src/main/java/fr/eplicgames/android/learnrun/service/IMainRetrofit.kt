package fr.eplicgames.android.learnrun.service

import retrofit2.Retrofit

interface IMainRetrofit {
    val retrofit: Retrofit
}