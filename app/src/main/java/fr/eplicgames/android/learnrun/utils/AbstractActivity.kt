package fr.eplicgames.android.learnrun.utils

import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.fragment.ILoginListener
import fr.eplicgames.android.learnrun.model.IUserContainer
import fr.eplicgames.android.learnrun.model.user.info.IUserModel
import fr.eplicgames.android.learnrun.persistentcookiestore.KtPersistentCookieStore
import fr.eplicgames.android.learnrun.service.IMainRetrofit
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy


abstract class AbstractActivity :
    AppCompatActivity(),
    IUserContainer,
    IMainRetrofit,
    ILoginListener {

    abstract val apiUrl: String
    abstract val layoutResource: Int

    override lateinit var user: IUserModel
    override lateinit var retrofit: Retrofit

    lateinit var cookieManager: CookieManager
    lateinit var jsonConverter: GsonConverterFactory
    lateinit var gson: Gson

    inline fun <reified T> service(): T = retrofit.create(T::class.java)

    abstract fun onInit()

    fun toast(msg: String) =
        Toast.makeText(this.applicationContext, msg, Toast.LENGTH_SHORT).show()

    fun toastLong(msg: String) =
        Toast.makeText(this.applicationContext, msg, Toast.LENGTH_LONG).show()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)

        setupRetrofitWithCookieAndJson(true)

        onInit()
    }

    inline fun <reified T> gsonRead(jsonString: String): T =
        gson.fromJson(jsonString, T::class.java)

    inline fun <reified T> gsonRead(jsonElement: JsonElement): T =
        gsonRead(jsonElement.asString)


    fun setupRetrofitWithCookieAndJson(areCookiesPersistent: Boolean) {
        cookieManager =
            if (areCookiesPersistent)
                CookieManager(KtPersistentCookieStore(this), CookiePolicy.ACCEPT_ALL)
            else
                CookieManager().apply { setCookiePolicy(CookiePolicy.ACCEPT_ALL) }


        val cookieJar = JavaNetCookieJar(cookieManager)
        val builder = OkHttpClient.Builder()
        builder.cookieJar(cookieJar)

        gson = GsonBuilder().create()
        jsonConverter = GsonConverterFactory.create(gson)
        val client = builder.build()

        retrofit = Retrofit.Builder()
            .baseUrl(apiUrl)
            .addConverterFactory(jsonConverter)
            .client(client)
            .build()
    }


    inline fun <reified T : FragmentActivityAccess> pushFragment(addToBackStack: Boolean = true) =
        supportFragmentManager
            .beginTransaction()
            .apply { if (addToBackStack) this.addToBackStack(null) }
            .replace(
                R.id.main_container,
                T::class.java.getConstructor(AbstractActivity::class.java)
                    .newInstance(this)
            )
            .commit()

    inline fun <reified T : FragmentActivityAccess> pushFragment(fragment: T, addToBackStack: Boolean = true) =
        supportFragmentManager
            .beginTransaction()
            .apply { if (addToBackStack) this.addToBackStack(null) }
            .replace(
                R.id.main_container,
                fragment
            )
            .commit()


    fun clearCookie() = cookieManager.cookieStore.removeAll()

    fun backActivity() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
    }

    fun resetApp() {
        clearBackStack()
        onInit()
    }

    fun clearBackStack() {
        while (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStackImmediate()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()
    }

    fun hideKeyBoard(): Boolean {
        val v: View? = currentFocus
        if (v != null) {
            val imm: InputMethodManager? =
                ContextCompat.getSystemService(applicationContext!!, InputMethodManager::class.java)
            imm?.hideSoftInputFromWindow(v.windowToken, 0)
        }
        return true
    }

}