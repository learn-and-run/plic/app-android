package fr.eplicgames.android.learnrun.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.model.holder.CircuitScore
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.row_home_student.view.*

class HomeRecyclerAdapter(
    val context: Context,
    val data: MutableList<CircuitScore>,
    val onItemClick: (position: Int, circuit: CircuitScore, view: View) -> Unit) :
    RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder>() {
    // the new RecyclerAdapter enforces the use of
    // the ViewHolder class performance pattern
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dotText: TextView = itemView.textViewDot
        val circuitName: TextView = itemView.textCircuitHomeStudent
        val score: TextView = itemView.textScoreHomeStudent
        val percent: TextView = itemView.percentTextScoreHomeStudent
    }
    override fun getItemCount(): Int {
        return data.size
    }
    // called when a new viewholder is required to display a row
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : ViewHolder {
        // create the row from a layout inflater
        val rowView = LayoutInflater
            .from(context)
            .inflate(R.layout.row_home_student, parent, false)
        rowView.setPushAndOnClick {
            val position = it.tag as Int
            onItemClick(position, data[position], it)
        }
        // create a ViewHolder using this rowview
        val viewHolder =
            ViewHolder(
                rowView
            )
        // return this ViewHolder. The system will make sure view holders
        // are used and recycled
        return viewHolder
    }
    // called when a row is about to be displayed
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // retrieve the item at the specified position
        val currentItem = data[position]
        // put the data
        holder.dotText.setTextColor(currentItem.moduleType.color)
        holder.circuitName.setTextColor(currentItem.moduleType.color)
        holder.score.setTextColor(currentItem.moduleType.color)
        holder.percent.setTextColor(currentItem.moduleType.color)
        holder.circuitName.text = currentItem.circuitName
        holder.score.text = currentItem.score.toString()

        holder.itemView.tag = position
    }
}