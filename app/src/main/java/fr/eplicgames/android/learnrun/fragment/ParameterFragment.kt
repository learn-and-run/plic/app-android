package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.service.IUserService
import fr.eplicgames.android.learnrun.service.call
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_parameter.*

/**
 * A simple [Fragment] subclass.
 */
class ParameterFragment(abstractActivity: AbstractActivity) :
    FragmentActivityAccess(abstractActivity) {
    override val layoutResource = R.layout.fragment_parameter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        parameter_logout.setPushAndOnClick {
            //TODO
            service<IUserService>().logout().call {
                onSuccess = {
                    mainActivity.resetApp()
                }
                onFailure = { toast(getString(R.string.cant_contact_server)) }
                onAnyErrorNoArg = {
                    mainActivity.resetApp()
                }
            }
        }
    }
}