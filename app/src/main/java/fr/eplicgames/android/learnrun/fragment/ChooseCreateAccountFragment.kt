package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import android.view.View
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_choose_create_account.*

class ChooseCreateAccountFragment(a: AbstractActivity) : FragmentActivityAccess(a) {

    override val layoutResource = R.layout.fragment_choose_create_account

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        children_button.setPushAndOnClick {
            mainActivity.pushFragment<CreateAccountChildFragment>()
        }

        parent_button.setPushAndOnClick {
            mainActivity.pushFragment<CreateAccountParentFragment>()
        }

    }

}
