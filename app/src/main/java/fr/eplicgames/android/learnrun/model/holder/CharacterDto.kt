package fr.eplicgames.android.learnrun.model.holder

class CharacterDto(
    val id: Long,
    val name: String
)