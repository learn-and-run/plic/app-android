package fr.eplicgames.android.learnrun.model.user.creation

import fr.eplicgames.android.learnrun.model.LevelType

data class StudentCreationModel (
    override val pseudo: String,
    override val firstname: String,
    override val lastname: String,
    override val password: String,
    override val email: String,
    val levelType: LevelType
) : IUserCreationModel
