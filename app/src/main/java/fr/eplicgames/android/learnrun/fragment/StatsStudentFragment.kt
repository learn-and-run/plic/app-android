package fr.eplicgames.android.learnrun.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.adapter.SpinnerArrayAdapter
import fr.eplicgames.android.learnrun.adapter.HomeRecyclerAdapter
import fr.eplicgames.android.learnrun.decoration.SimpleDividerItemDecoration
import fr.eplicgames.android.learnrun.design.spinner.CustomSpinner
import fr.eplicgames.android.learnrun.model.ModuleType
import fr.eplicgames.android.learnrun.model.holder.CircuitScore
import fr.eplicgames.android.learnrun.service.IScoreService
import fr.eplicgames.android.learnrun.service.IUserService
import fr.eplicgames.android.learnrun.service.call
import fr.eplicgames.android.learnrun.utils.AbstractActivity
import fr.eplicgames.android.learnrun.utils.FragmentActivityAccess
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.fragment_stats_student.*

/**
 * A simple [Fragment] subclass.
 */
class StatsStudentFragment(abstractActivity: AbstractActivity, val pseudo: String) :
    FragmentActivityAccess(abstractActivity) {

    override val layoutResource = R.layout.fragment_stats_student

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        stats_class_spinnerHomeStudent.setPopupBackgroundDrawable(
            resources.getDrawable(R.drawable.spinner_back_activate, context?.theme)
        )
        stats_class_spinnerHomeStudent.dropDownVerticalOffset = 10.dp()

        val adapter = SpinnerArrayAdapter(
            context!!,
            stats_spinnerTextViewHomeStudent,
            ModuleType.values().toList(),
            ModuleType::displayName,
            ModuleType::color
        ) { moduleType ->
            service<IScoreService>().getCircuitsScores("Bigfoot"/*TODO put pseudo*/, moduleType).call {
                onSuccess = {
                    stats_listHomeStudent.adapter =
                        HomeRecyclerAdapter(
                            mainActivity,
                            it.body()?.scores?.toMutableList() ?: mutableListOf()
                        ) { row: Int, circuitScore: CircuitScore, _: View ->
                            toast("row = $row\ncircuitScore = $circuitScore")
                        }
                }
            }
        }
        stats_class_spinnerHomeStudent.adapter = adapter


        stats_class_spinnerHomeStudent.setSpinnerEventsListener(object :
            CustomSpinner.OnSpinnerEventsListener {
            override fun onSpinnerOpened(spinner: AppCompatSpinner?) {
                spinnerActivate()
            }

            override fun onSpinnerClosed(spinner: AppCompatSpinner?) {
                spinnerDeactivate()
            }
        })

        stats_listHomeStudent.addItemDecoration(
            SimpleDividerItemDecoration(
                ContextCompat.getColor(context!!, R.color.colorPrimaryDark),
                2
            )
        )

        stats_listHomeStudent.setHasFixedSize(true)
        stats_listHomeStudent.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )

        button_stats_back.setPushAndOnClick {
            mainActivity.backActivity()
        }

    }

    fun spinnerDeactivate() {
        stats_class_spinnerHomeStudent.background =
            resources.getDrawable(R.drawable.spinner_back_deactivate, context?.theme)
        //spinnerTextViewHomeStudent.visibility = View.VISIBLE
        stats_class_spinnerHomeStudent.background.setTint(ModuleType.values()[stats_class_spinnerHomeStudent.selectedItemPosition].color)
        stats_chevronHomeStudent.setImageDrawable(resources.getDrawable(R.drawable.chevron_bottom, context?.theme))
    }

    fun spinnerActivate() {
        //class_spinnerHomeStudent.background =
        //    resources.getDrawable(R.drawable.spinner_back_activate, context?.theme)
        //spinnerTextViewHomeStudent.visibility = View.INVISIBLE
        stats_chevronHomeStudent.setImageDrawable(resources.getDrawable(R.drawable.chevron_top, context?.theme))
    }
}
