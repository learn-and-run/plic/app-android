package fr.eplicgames.android.learnrun.persistentcookiestore

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.net.CookieStore
import java.net.HttpCookie
import java.net.URI
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

//Inspiration: https://gist.github.com/jacobtabak/78e226673d5a6a4c4367
class KtPersistentCookieStore(context: Context) : CookieStore {
    companion object {
        private const val COOKIE_PREFS = "CookiePrefsFile"
    }


    data class SimpleCookie (
        val url: String,
        val name: String,
        val value: String
    )

    private var simpleCookieList = mutableSetOf<SimpleCookie>()
    /**
     * Map of host and cookies associated
     * The cookies are store with a map of their names and their value
     */
    val hostCookiesMap: ConcurrentMap<String, ConcurrentHashMap<String, HttpCookie>> = ConcurrentHashMap()
    private val cookiePrefs: SharedPreferences

    init {
        cookiePrefs = context.getSharedPreferences(COOKIE_PREFS, 0)
        val map = cookiePrefs.getString("simpleCookieMap", null)

        if (map != null) {
            simpleCookieList = ObjectMapper().registerKotlinModule().readValue(map)
            simpleCookieList.forEach {
                addCookie(it.url, HttpCookie(it.name, it.value))
            }
        }
    }

    private fun addCookie(url: String, cookie: HttpCookie) {
        if (cookie.hasExpired())
            return

        hostCookiesMap.putIfAbsent(url, ConcurrentHashMap())
        val map = hostCookiesMap[url]
            ?: throw IllegalStateException("This value should not be null")

        map[cookie.computeToken()] = cookie
    }

    override fun add(uri: URI?, cookie: HttpCookie?) {
        if (uri == null || cookie == null || cookie.hasExpired())
            return

        addCookie(uri.host, cookie)
        val toDel = simpleCookieList.filter { it.name == cookie.name }
        toDel.forEach { simpleCookieList.remove(it) }
        simpleCookieList.add(SimpleCookie(uri.host, cookie.name, cookie.value))

        savePrefs()
    }

    override fun getCookies(): MutableList<HttpCookie> =
        hostCookiesMap.map { it.value }.flatMap { it.values }.toMutableList()

    override fun get(uri: URI?): MutableList<HttpCookie> =
        if (uri == null)
            mutableListOf()
        else
            hostCookiesMap[uri.host]?.map { it.value }?.toMutableList()
                ?: mutableListOf()

    override fun getURIs(): MutableList<URI> = hostCookiesMap.keys.map { URI(it) }.toMutableList()

    override fun remove(uri: URI?, cookie: HttpCookie?): Boolean {
        val ret = hostCookiesMap[uri?.host]?.remove(cookie?.computeToken()) != null
        val first = simpleCookieList.firstOrNull { it.name == cookie?.name }
        if (first != null)
            simpleCookieList.remove(first)
        savePrefs()
        return ret
    }

    override fun removeAll(): Boolean {
        hostCookiesMap.clear()
        simpleCookieList.clear()
        clearPrefs()
        return true
    }

    private fun clearPrefs() {
        val prefsWriter: SharedPreferences.Editor = cookiePrefs.edit()
        prefsWriter.clear()
        prefsWriter.apply()
    }

    private fun savePrefs() {
        clearPrefs()
        val prefsWriter = cookiePrefs.edit()
        val jsonMapper = ObjectMapper().registerKotlinModule()
        val map = jsonMapper.writeValueAsString(simpleCookieList)
        prefsWriter.putString("simpleCookieMap", map)
        prefsWriter.apply()
    }

    fun HttpCookie.computeToken() = this.name + this.domain


}