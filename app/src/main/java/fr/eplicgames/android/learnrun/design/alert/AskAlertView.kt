package fr.eplicgames.android.learnrun.design.alert

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.app.AlertDialog
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.alert_ask_dialog.view.*
import kotlinx.android.synthetic.main.alert_ask_dialog.view.alert_text_view

class AskAlertView
@JvmOverloads
constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AlertView(context, attrs, defStyleAttr) {

    var onYes: () -> Unit = {}
    var onNo: () -> Unit = {}

    lateinit var alertDialog: AlertDialog

    init {
        inflate(context, R.layout.alert_ask_dialog, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.AskAlertView)
        alert_text_view.text = attributes.getString(R.styleable.AskAlertView_android_text)
        buttonYes.text = attributes.getString(R.styleable.AskAlertView_yesText)
            ?: resources.getString(R.string.yes)
        buttonNo.text = attributes.getString(R.styleable.AskAlertView_noText)
            ?: resources.getString(R.string.no)
        attributes.recycle()

        buttonYes.setPushAndOnClick { alertDialog.dismiss(); onYes() }
        buttonNo.setPushAndOnClick { alertDialog.dismiss(); onNo() }
    }

    override var text: String
        get() = alert_text_view?.text as? String ?: ""
        set(value) {
            alert_text_view.text = value
        }


    var textYes: String
        get() = buttonYes.text
        set(value) {
            buttonYes.text = value
        }


    var textNo: String
        get() = buttonNo.text
        set(value) {
            buttonNo.text = value
        }

}