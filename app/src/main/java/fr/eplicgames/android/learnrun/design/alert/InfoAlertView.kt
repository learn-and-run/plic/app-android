package fr.eplicgames.android.learnrun.design.alert

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.app.AlertDialog
import fr.eplicgames.android.learnrun.R
import fr.eplicgames.android.learnrun.utils.setPushAndOnClick
import kotlinx.android.synthetic.main.alert_info_dialog.view.*

class InfoAlertView
@JvmOverloads
constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AlertView(context, attrs, defStyleAttr) {

    var onOk: () -> Unit = {}

    lateinit var alertDialog: AlertDialog

    init {
        inflate(context, R.layout.alert_info_dialog, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.InfoAlertView)
        alert_text_view.text = attributes.getString(R.styleable.InfoAlertView_android_text)
        buttonUnderstood.text = attributes.getString(R.styleable.InfoAlertView_okText)
            ?: resources.getString(R.string.understood)
        attributes.recycle()

        buttonUnderstood.setPushAndOnClick { alertDialog.dismiss(); onOk() }
    }

    override var text: String
        get() = alert_text_view?.text as? String ?: ""
        set(value) {
            alert_text_view.text = value
        }


    var textOk: String
        get() = buttonUnderstood.text
        set(value) {
            buttonUnderstood.text = value
        }

}